#include "ctrackgenerate.h"
#include "ui_ctrackgenerate.h"


CTrackGenerate::CTrackGenerate(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CTrackGenerate)
{
    ui->setupUi(this);


    this->setWindowTitle("位控模式位置曲线");
    SetPageShow(false);

    // 部分控件的属性
    ui->num_sineWave_ampl->setRange(0, 359);
    ui->num_chirpWave_freqBegin->setRange(0, 600);
    ui->num_chirpWave_freqEnd->setRange(1, 600);

    // 保存曲线数据的容器
    m_vector_posCurveData = new QVector<float>;

    // 以固定频率发送指令的定时器
    m_timer_sendPos = new QTimer(this);
    connect(m_timer_sendPos, &QTimer::timeout, this, &CTrackGenerate::slot_tim_SendPos_Timeout);

    // 绑定曲线预览和运行的信号槽
    connect(ui->btn_curvePreview, &QPushButton::clicked, this, &CTrackGenerate::slot_Btn_CurvePreview_Cliecked);
    connect(ui->btn_run, &QPushButton::clicked, this, &CTrackGenerate::slot_Btn_MotorRun_Cliecked);

    // 绑定曲线类型下拉列表的信号槽
    connect(ui->cbox_curveType, &QComboBox::currentTextChanged, this, [=]{
        QString curveType = ui->cbox_curveType->currentText();
        if (curveType == "正弦")
        {
            ui->stackedWidget->setCurrentWidget(ui->page_sineWave);
        }
        if (curveType == "三角波")
        {
            ui->stackedWidget->setCurrentWidget(ui->page_triangleWave);
        }
        if (curveType == "扫频")
        {
            ui->stackedWidget->setCurrentWidget(ui->page_chirpWave);
        }
    });
}


CTrackGenerate::~CTrackGenerate()
{
    delete ui;
}

QVector<float> *CTrackGenerate::GetPositionCurveData(float intervalMs)
{
    float ampl = 0;
    float freq = 0;
    float freqEnd = 0;
    float phase = 0;
    float cycle = 0;
    float time = 0;
    int points = 0;
    int ret = 0;

    // 判断波形类型
    QString curveType = ui->cbox_curveType->currentText();
    if (curveType == "正弦")
    {
        ampl = ui->num_sineWave_ampl->value();
        freq = ui->num_sineWave_freq->value();
        phase = ui->num_sineWave_phase->value();
        cycle = ui->num_sineWave_cycle->value();

        //points = SG_GetWavePointNum(freq, cycle, m_sendCmdIntervalMs / 1000.0);
    }
    if (curveType == "三角波")
    {
        ampl = ui->num_triangleWave_ampl->value();
        freq = ui->num_triangleWave_freq->value();
        cycle = ui->num_triangleWave_cycle->value();

        //points = SG_GetWavePointNum(freq, cycle, m_sendCmdIntervalMs / 1000.0);
    }
    if (curveType == "扫频")
    {
        ampl =      ui->num_chirpWave_ampl->value();
        freq =      ui->num_chirpWave_freqBegin->value();
        freqEnd =   ui->num_chirpWave_freqEnd->value();
        time =      ui->num_chirpWave_time->value();

        points = (1.0 / (m_sendCmdIntervalMs / 1000.0)) * time;
    }

    qDebug() << "GetPositionCurveData()" << curveType << ampl << freq << freqEnd << phase << cycle << points << time;

    // 制作波形
    float* data = (float*)calloc(points, sizeof(float));
    if (points <= 0 || data == NULL)
        return nullptr;

    if (curveType == "正弦")
    {
        //ret = SG_SineWave(ampl ,freq, phase, cycle, m_sendCmdIntervalMs / 1000.0, data);
    }
    if (curveType == "三角波")
    {
        //ret = SG_TriangleWave(ampl, freq, cycle, m_sendCmdIntervalMs / 1000.0, NULL, data);
    }
    if (curveType == "扫频")
    {
        //ret = SG_ChirpWave(ampl, freq, freqEnd, m_sendCmdIntervalMs / 1000.0, time, data);
    }
    if (ret <= 0)
    {
        free(data);
        return nullptr;
    }

    // 把角度转换为转，0~360(°) -> 0~1(turn)
    m_vector_posCurveData->clear(); // 先删除旧数据
    for (int i = 0; i < points; i++)
    {
        m_vector_posCurveData->append(fmodf(data[i], 360.0f) / 360.0);
    }
    free(data);

    // 判断是否启用了首尾加0位置段
    bool isAddZero = ui->cbox_curveAddZeroPosSection->isChecked();
    if (isAddZero)
    {
        constexpr int pointNum = 50;
        float zeroPos[pointNum] = {0};

        for (int i = 0; i < pointNum; i++)
            m_vector_posCurveData->push_front(zeroPos[i]);
        for (int i = 0; i < pointNum; i++)
            m_vector_posCurveData->push_back(zeroPos[i]);
    }

    return m_vector_posCurveData;
}

void CTrackGenerate::slot_Btn_CurvePreview_Cliecked()
{
    // 获取波形数据数据
    GetPositionCurveData(m_sendCmdIntervalMs);

    int points = m_vector_posCurveData->length();

    // 显示波形
    QVector<double> key;
    for (int i = 0; i < points; i++)
    {
        key.append(i);
    }
    QVector<double> val;
    for (int i = 0; i < points; i++)
    {
        val.append(m_vector_posCurveData->at(i));
    }


    ui->qcp_scope->clearGraphs(); // 删除旧图像
    ui->qcp_scope->addGraph();
    ui->qcp_scope->graph(0)->addData(key, val);

    ui->qcp_scope->xAxis->setRange(0, points);
    ui->qcp_scope->yAxis->setLabel("转(Turn)");
    ui->qcp_scope->yAxis->rescale(true);

    float ampl = ui->num_sineWave_ampl->value();
    ui->qcp_scope->yAxis2->setVisible(true);
    ui->qcp_scope->yAxis2->setLabel("角度(°)");
    ui->qcp_scope->yAxis2->setRange(-ampl, ampl);

    ui->qcp_scope->replot();
}

void CTrackGenerate::slot_Btn_MotorRun_Cliecked()
{
    // 判断是否有数据
    if (m_vector_posCurveData->length() <= 0)
    {
        qDebug() << "未制作曲线" << "即将自动根据面板数据生成曲线";
        GetPositionCurveData(m_sendCmdIntervalMs);
    }

    // 使能电机

    // 启动定时器
    m_posCurveDataIdx = 0; // 数据索引归零
    m_timer_sendPos->start(m_sendCmdIntervalMs);
}

void CTrackGenerate::slot_tim_SendPos_Timeout()
{
    if (m_posCurveDataIdx >= m_vector_posCurveData->length())
    {
        m_timer_sendPos->stop();
        return;
    }
    emit signal_SendPos(m_vector_posCurveData->at(m_posCurveDataIdx++));
}
