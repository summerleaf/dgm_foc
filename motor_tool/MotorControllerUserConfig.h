#ifndef MOTORCONTROLLERUSERCONFIG_H
#define MOTORCONTROLLERUSERCONFIG_H


#include <stdint.h>


#define FW_VERSION_MAJOR 3
#define FW_VERSION_MINOR 2

#define OFFSET_LUT_NUM   128
#define COGGING_MAP_NUM  5000

typedef enum {
    CAN_BAUDRATE_250K = 0,
    CAN_BAUDRATE_500K,
    CAN_BAUDRATE_800K,
    CAN_BAUDRATE_1000K,
} tCanBaudrate;

typedef struct sUsrConfig
{
    // Motor
    int32_t invert_motor_dir;       // 0 False : 1 True
    float   inertia;                // [A/(turn/s^2)]       (0~100)
    float   torque_constant;        // [Nm/A]               (0~10)
    int32_t motor_pole_pairs;       // [PP]                 (2~30)
    float   motor_phase_resistance; // [R]                  (0~10)
    float   motor_phase_inductance; // [H]                  (0~10)
    float   current_limit;          // [A]                  (0~45)
    float   velocity_limit;         // [turn/s]             (0~100)

    // Calibration
    float calib_current;            // [A]                  (0~45)
    float calib_voltage;            // [V]                  (0~50)

    // Controller
    int32_t control_mode;
    float   pos_gain;               //                      (0~1000)
    float   vel_gain;               //                      (0~1000)
    float   vel_integrator_gain;    //                      (0~1000)
    float   current_ctrl_bw;        //                      (2~60000)
    int32_t anticogging_enable;     // 0 False : 1 True
    int32_t sync_target_enable;     // 0 False : 1 True
    float   target_velcity_window;  // [turn/s]             (0~100)
    float   target_position_window; // [turn]               (0~100)
    float   torque_ramp_rate;       // [Nm/s]               (0~100)
    float   velocity_ramp_rate;     // [(turn/s)/s]         (0~1000)
    float   position_filter_bw;     //                      (2~5000)
    float   profile_velocity;       // [turn/s]             (0~100)
    float   profile_accel;          // [(turn/s)/s]         (0~1000)
    float   profile_decel;          // [(turn/s)/s]         (0~1000)

    // Protect
    float protect_under_voltage;    // [V]                  (0~50)
    float protect_over_voltage;     // [V]                  (0~50)
    float protect_over_current;     // [A]                  (0~45)
    float protect_i_bus_max;        // [A]                  (0~10)

    // CAN
    int32_t node_id;                //                      (1~31)
    int32_t can_baudrate;           // REF: tCanBaudrate
    int32_t heartbeat_consumer_ms;  // rx heartbeat timeout in ms : 0 Disable       (0~600000)
    int32_t heartbeat_producer_ms;  // tx heartbeat interval in ms : 0 Disable      (0~600000)

    // Encoder
    int32_t calib_valid;                // (Auto)
    int32_t encoder_dir;                // (Auto)
    int32_t encoder_offset;             // (Auto)
    int32_t offset_lut[OFFSET_LUT_NUM]; // (Auto)

    uint32_t crc;
} tUsrConfig;


#endif // MOTORCONTROLLERUSERCONFIG_H
