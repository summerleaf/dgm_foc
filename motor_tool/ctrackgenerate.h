#ifndef CTRACKGENERATE_H
#define CTRACKGENERATE_H

#include <QWidget>

namespace Ui {
class CTrackGenerate;
}

class CTrackGenerate : public QWidget
{
    Q_OBJECT

public:
    explicit CTrackGenerate(QWidget *parent = nullptr);
    //explicit CTrackGenerate(QWidget *parent = nullptr);
    ~CTrackGenerate();

    QVector<float>* GetPositionCurveData(float intervalMs = 2.0);

    void SetPageShow(bool visible)
    {
        this->setVisible(visible);
    }


signals:
    void signal_SendPos(float val);


private slots:
    void slot_Btn_CurvePreview_Cliecked();
    void slot_Btn_MotorRun_Cliecked();

    void slot_tim_SendPos_Timeout();


private:
    Ui::CTrackGenerate *ui;

    const float m_sendCmdIntervalMs = 2.0f;

    int m_posCurveDataIdx = 0;
    QVector<float>* m_vector_posCurveData;
    QTimer* m_timer_sendPos;
};

#endif // CTRACKGENERATE_H
