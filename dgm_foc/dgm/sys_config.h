/*
    Copyright 2021 codenocold codenocold@qq.com
    Address : https://github.com/codenocold/dgm
    This file is part of the dgm firmware.
    The dgm firmware is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    The dgm firmware is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __SYS_CONFIG__
#define __SYS_CONFIG__

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

extern volatile uint32_t SystickCount;

// LED ACT
#define LED_ACT_SET()       HAL_GPIO_WritePin(Led1_GPIO_Port, Led1_Pin, GPIO_PIN_SET)
#define LED_ACT_RESET()     HAL_GPIO_WritePin(Led1_GPIO_Port, Led1_Pin, GPIO_PIN_RESET)
#define LED_ACT_TOGGLE()    HAL_GPIO_TogglePin(Led1_GPIO_Port, Led1_Pin)

// SPI NCS
#define NCS_SET()   HAL_GPIO_WritePin(Encoder_CS_GPIO_Port, Encoder_CS_Pin, GPIO_PIN_SET)
#define NCS_RESET() HAL_GPIO_WritePin(Encoder_CS_GPIO_Port, Encoder_CS_Pin, GPIO_PIN_RESET)

// IO
#define IO_SET()    HAL_GPIO_WritePin(UserTest_GPIO_Port, UserTest_Pin, GPIO_PIN_SET)
#define IO_RESET()  HAL_GPIO_WritePin(UserTest_GPIO_Port, UserTest_Pin, GPIO_PIN_RESET)
#define IO_TOGGLE() HAL_GPIO_TogglePin(UserTest_GPIO_Port, UserTest_Pin)


/* FLASH MAP ---------------------------------------------*/
#define PAGE_SIZE            ((uint32_t) 2048) // 2KB

#define APP_MAIN_PAGE           0
#define APP_MAIN_PAGE_SIZE      80
#define APP_MAIN_ADDR           ((uint32_t) (FLASH_BASE + APP_MAIN_PAGE * PAGE_SIZE))

#define USR_CONFIG_PAGE         90
#define USR_CONFIG_PAGE_SIZE    5
#define USR_CONFIG_ADDR         ((uint32_t) (FLASH_BASE + USR_CONFIG_PAGE * PAGE_SIZE))
#define USR_CONFIG_MAX_SIZE     ((uint32_t) (USR_CONFIG_PAGE_SIZE * PAGE_SIZE))

#define COGGING_MAP_PAGE        100
#define COGGING_MAP_PAGE_SIZE   15
#define COGGING_MAP_ADDR        ((uint32_t) (FLASH_BASE + COGGING_MAP_PAGE * PAGE_SIZE))
#define COGGING_MAP_MAX_SIZE    ((uint32_t) (COGGING_MAP_PAGE_SIZE * PAGE_SIZE))



/* Exported functions prototypes ---------------------------------------------*/
static inline void watch_dog_feed(void)
{
//    FWDGT_CTL = FWDGT_KEY_RELOAD;
}

static inline uint32_t get_ms_since(uint32_t tick)
{
    return (uint32_t) ((SystickCount - tick) / 2U);
}


#ifdef __cplusplus
}
#endif

#endif /* __SYS_CONFIG__ */
